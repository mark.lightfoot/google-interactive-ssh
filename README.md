# Google Interactive SSH

**DISCLAIMER: This is NOT endorsed by Google or any such thing. Using this is at your own risk and I take no liability for damages caused.**

## gsshi

Interactive prompt for GCP to ssh into appropriate boxes. Will detect if the box has an external IP and connect, or if not find a NAT box within the project to use. It also uses a separate knownhosts file (knownhosts-projectid) for each GCP project.

### Flags

Usage: `gsshi`

* -h            - help
* -n {nat}      - NAT Suffix to look for. if an instance is called management-client-nat-2dg4, it detects it. Defaults to `nat`.
* -p {proj}     - Projectname or part of to filter for. If a specific projectname it will just use that.
* -s {options}  - Add any extra SSH flags needed



##Credit to Ben Pollard (@ben.pollard) for the idea
